#!/usr/bin/python
#
# Copyright 2015 Christian Forssen
#
#  This file is part of WIGXJPF.
#
#  WIGXJPF is free software: you can redistribute it and/or modify it
#  under the terms of the GNU Lesser General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  WIGXJPF is distributed in the hope that it will be useful, but
#  WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with WIGXJPF.  If not, see
#  <http://www.gnu.org/licenses/>.
#

import pywigxjpf as wig
from numba import complex128,float64,int64,int32,jit
import numpy as np
import time

print( 'WIGXJPF python test program')

# Initialize
wig.wig_table_init(2*100,9)
print("test")
wig.wig_temp_init(2*100)
#wig.wig_temp_init(2*100)

# Note that arguments are in two_j = 2*j.
val3j = wig.pywigxjpf._wig3jj(2* 10 , 2* 15 , 2* 10 ,\
                    2*(-3), 2* 12 , 2*(-9))
print( '3J(10  15  10; -3  12  -9):', val3j)

val3j = wig.wig3jj([2* 10 , 2* 15 , 2* 10 ,\
                    2*(-3), 2* 12 , 2*(-9)])
print( '3J(10  15  10; -3  12  -9):', val3j)

val6j = wig.wig6jj([2* 10 , 2* 15 , 2* 10 ,\
                    2*  7,  2*  7 , 2*  9 ])
print( "6J{10  15  10;  7   7   9}:", val6j)

val9j = wig.wig9jj( [1,  2,  3,\
                     4,  6,  8, \
                     3,  6,  9] )
print( "9J{0.5 1 1.5; 2 3 4; 1.5 3 4.5}:", val9j)

#speed test
def benchmark(jmax):
    for m1 in range(-jmax, jmax + 1):
        for m2 in range(-jmax, jmax + 1):
            for m3 in range(-jmax, jmax + 1):
                for j1 in range(1, jmax + 1):
                    for j2 in range(1, jmax + 1):
                        for j3 in range(1, jmax + 1):
                            wig1 = wig.wig3jj([2*j1, 2*j2, 2*j3,\
                                               2*m1, 2*m2, 2*m3])
                            # if wig1>0: print(wig1)
    return wig1

#speed test
#@jit(float64(int64),cache=True)
def benchmark2(jmax):
    for j1 in range(1, jmax + 1):
        for j2 in range(1, jmax + 1):
            for j3 in range(1, jmax + 1):
                for m1 in range(-jmax, jmax + 1):
                    for m2 in range(-jmax, jmax + 1):
                        for m3 in range(-jmax, jmax + 1):
                            wig1 = wig.pywigxjpf._wig3jj(2*j1, 2*j2, 2*j3,\
                                                         2*m1, 2*m2, 2*m3)
                            if (wig1 != 0.0):
                                print(wig1)
    return wig1

@jit(int32[:](int32),cache=True)
def benchmark3(jmax):
    size = (jmax)**3 * (2*jmax+1)**3
    arg = np.zeros((size*6), dtype=np.int32)
    i=0
    for j1 in range(1, jmax + 1):
        for j2 in range(1, jmax + 1):
            for j3 in range(1, jmax + 1):
                for m1 in range(-jmax, jmax + 1):
                    for m2 in range(-jmax, jmax + 1):
                        for m3 in range(-jmax, jmax + 1):
                            # arg[i*6 : i*6+6] = j1, j2, j3, m1, m2, m3
                            arg[i*6  ] = j1; arg[i*6+1] = j2; arg[i*6+2] = j3
                            arg[i*6+3] = m1; arg[i*6+4] = m2; arg[i*6+5] = m3
                            i+=1
    return arg

jmax = 10
start_time = time.time()

#wig1 = benchmark(jmax)
#wig1 = benchmark2(jmax)

arg = benchmark3(jmax)
#print(arg)
wig3jj_table = np.zeros(arg.size//6)
wig.pywigxjpf._wig3j_table(wig3jj_table, wig3jj_table.size, arg)
#print(wig3jj_table[np.nonzero(wig3jj_table)])

#wig1 = benchmark2(jmax)
total_time = time.time()-start_time
print("Benchmark for jmax=", jmax,"total_time = ", total_time)
#print("size: ", wig3jj_table.size )
# Free memory space
wig.wig_temp_free()
wig.wig_table_free()


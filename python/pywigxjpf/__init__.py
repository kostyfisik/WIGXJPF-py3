import sys
if sys.version_info.major >= 3:
    from pywigxjpf.pywigxjpf import wig_table_init, wig_temp_init, wig_temp_free, wig_table_free, wig3jj, wig6jj, wig9jj
else:
    from pywigxjpf import wig_table_init, wig_temp_init, wig_temp_free, wig_table_free, wig3jj, wig6jj, wig9jj



